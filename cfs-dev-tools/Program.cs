﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CFS.PackageCreator
{
    class Program
    {
        static void Main(string[] args)
        {
            //load username and password with default values (ILT package org)
            String username = ConfigurationManager.AppSettings["Username"];
            String password = ConfigurationManager.AppSettings["Password"];

            //overwrite default values if username and password are provided
            if (args.Length > 0)
            {
                username = args[0];
                if (args.Length > 1) 
                {
                    password = args[1];
                }
            }
            System.Console.WriteLine("Starting the package creation process");
            CreateSfdcPackage.createSfdcPackage(username, password);
        }
    }
}
