﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System.Configuration;

namespace CFS.PackageCreator
{
    class CreateSfdcPackage
    {
        public static void createSfdcPackage(String username, String password)
        {
            int timeoutMinutes = Convert.ToInt32(ConfigurationManager.AppSettings["TimeoutMinutes"]);
            IWebDriver driver = new ChromeDriver(ChromeDriverService.CreateDefaultService(), new ChromeOptions(), TimeSpan.FromMinutes(timeoutMinutes));
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMinutes(timeoutMinutes);
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromMinutes(timeoutMinutes);
            driver.Url = "https://login.salesforce.com";
            
            System.Console.WriteLine("Logging in the package org with username: " + username);
            driver.FindElement(By.Name("username")).SendKeys(username);
            driver.FindElement(By.Name("pw")).SendKeys(password);
            driver.FindElement(By.Name("Login")).Submit();
            
            IWebElement packageLink = driver.FindElement(By.CssSelector("a[href$='?tab=PackageComponents']"));
            System.Console.WriteLine("Successfully logged in. Navigating to the package page.");
            packageLink.Click();

            //check if the recompileAll button is there and click it if it is
            if (driver.PageSource.Contains("BadExternalRefs"))
            {
                IWebElement recompileAllButton = driver.FindElement(By.CssSelector("input[type=submit][value='Recompile All']"));
                System.Console.WriteLine("Clicking on Recompile All button.");
                recompileAllButton.Click();
            }
            
            IWebElement uploadButton = driver.FindElement(By.CssSelector("input[id$=upload]"));
            System.Console.WriteLine("Package page successfully loaded. Clicking on Upload button.");
            uploadButton.Click();

            String version = driver.FindElement(By.CssSelector("input[id$=VersionNumber]")).GetAttribute("value");
            driver.FindElement(By.CssSelector("input[id$=VersionText]")).SendKeys(version);
            driver.FindElement(By.CssSelector("input[id$='TypeRadio:0']")).Click();
            System.Console.WriteLine("Upload dialog successfully loaded. Uploading version: " + version);

            driver.FindElement(By.CssSelector("input[id$=upload]")).Click();
            driver.SwitchTo().Alert().Accept();
            driver.FindElement(By.CssSelector("form[id='ExportPackageDetailPage:theForm']"));
            System.Console.WriteLine("Upload is in progress. An email will be sent upon completion. Exiting Selenium.");
            
            driver.Quit();
        }
    }
}
